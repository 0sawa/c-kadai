#include <iostream>
#include "Player.h"

//コンストラクタ（初期化）
Player::Player()
{
	hp = 300; //HP
	atk = 50; //攻撃力
	def = 35; //防御力
}

//HPを表示
//引数 : なし
//戻り値 : なし
void Player::DispHp()
{
	std::cout << "プレイヤーHP = " << hp << "\n";
}

//攻撃
//引数 : 敵の防御力
//戻り値 : ダメージ量
int Player::Attack(int i)
{
	std::cout << "プレイヤーの攻撃！　";
	return atk - i / 2;
}

//ダメージを受ける
//引数 : 受けるダメージ
//戻り値 : なし
void Player::Damage(int i)
{
	std::cout << "プレイヤーは" << i << "のダメージ\n";
	hp -= i;
}

//防御力を取得(アクセス関数)
//引数 : なし
//戻り値 : 防御力
int Player::GetDef()
{
	return def;
}

//戦闘不能判定
//引数 : なし
//戻り値 : 戦闘不能 = true その他 = false
bool Player::IsDead()
{
	//HPが0以下だったらtureを返す
	if (hp <= 0)
	{
		return true;
	}
	//それ以外ならfalseを返す
	return false;
}