#include <iostream>
#include "Enemy.h"

//コンストラクタ（初期化）
Enemy::Enemy()
{
	hp = 200; //HP
	atk = 35; //攻撃力
	def = 40; //防御力
}

//HPを表示
//引数 : なし
//戻り値 : なし
void Enemy::DispHp()
{
	std::cout << "敵HP = " << hp << "\n";
}

//攻撃
//引数 : 敵の防御力
//戻り値 : ダメージ量
int Enemy::Attack(int i)
{
	std::cout << "敵の攻撃！　";
	return atk - i / 2;
}

//ダメージを受ける
//引数 : 受けるダメージ
//戻り値 : なし
void Enemy::Damage(int i)
{
	std::cout << "敵は" << i << "のダメージ\n";
	hp -= i;
}

//防御力を取得(アクセス関数)
//引数 : なし
//戻り値 : 防御力
int Enemy::GetDef()
{
	return def;
}

//戦闘不能判定
//引数 : なし
//戻り値 : 戦闘不能 = true その他 = false
bool Enemy::IsDead()
{
	//HPが0以下だったらtureを返す
	if (hp <= 0)
	{
		return true;
	}
	//それ以外ならfalseを返す
	return false;
}