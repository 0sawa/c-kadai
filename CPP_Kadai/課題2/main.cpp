#include "TriangleClass.h"

int main()
{
	//TriangleClassクラスのインスタンス
	TriangleClass t;

	//メンバ関数を呼び出す
	t.Input(); //TriangleClass a > TriangleClass > Input() を呼び出す
	t.Calc();
	t.Disp();
}