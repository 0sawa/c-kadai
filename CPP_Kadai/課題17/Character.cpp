#include "Character.h"
#include <iostream>
using namespace std;


//HPを表示
//引数 : なし
//戻り値 : なし
void Character::DispHp(const char* c)
{
	cout << c << "HP = " << hp << "\n";
}


//攻撃
//引数 : 敵の防御力
//戻り値 : ダメージ量
int Character::Attack(int i)
{
	std::cout << "攻撃！　";
	return atk - i / 2;
}


//ダメージを受ける
//引数 : 受けるダメージ
//戻り値 : なし
void Character::Damage(int i, const char* c)
{
	std::cout << c << "は" << i << "のダメージ\n";
	hp -= i;
}


//防御力を取得(アクセス関数)
//引数 : なし
//戻り値 : 防御力
int Character::GetDef()
{
	return def;
}


//戦闘不能判定
//引数 : なし
//戻り値 : 戦闘不能 = true その他 = false
bool Character::IsDead()
{
	//HPが0以下だったらtureを返す
	if (hp <= 0)
	{
		return true;
	}
	//それ以外ならfalseを返す
	return false;
}