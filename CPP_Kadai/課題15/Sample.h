/*
	Sample.h
	SampleClass クラスを宣言
*/

//クラスを宣言
class SampleClass
{
protected:
	//メンバ変数
	int a; //値を代入する
	int b; //値を代入する
	int c; //計算を行う

	//メンバ関数
public:
	void Input(); //値を代入するメンバ関数
	void Plus(); //計算を行うメンバ関数
	void Disp(); //出力するメンバ関数
};
